<?php

namespace App\Repositories;

interface SearchRepositoryInterface
{

    public function store($data);

    public function history();
}