<?php

namespace App\Repositories;
use App\Models\PreviousSearches;
use Illuminate\Support\Facades\DB;

class SearchRepository implements SearchRepositoryInterface
{


    /**
     * DESCRIPTION
     * ------------------
     * create a new record in the database
     * @param Array<$data>
     * @return Array/Object
     */
    public function store($data)
    {
        return PreviousSearches::create($data);
    }


    /**
     * DESCRIPTION
     * ------------------
     * show the record
     * @return Array/Object
     */
    public function history()
    {
        $conn = DB::connection()->getPdo();
        $stmtParams = array();
        $sqlWhere = "";
        $previousSearches = $conn->prepare(
            "
             SELECT 
                ps.`word`,
                ps.`created_at` AS timeSearched
             FROM `previous_searches` ps  
            " .
            $sqlWhere
        );
        $previousSearches->execute($stmtParams);
        return $previousSearches->fetchAll($conn::FETCH_ASSOC);
    }
}
