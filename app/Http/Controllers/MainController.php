<?php

namespace App\Http\Controllers;

use App\Repositories\SearchRepository;
use Illuminate\Http\Request;

class MainController extends Controller
{
    protected $searchRepo;

    public function __construct()
    {
        $this->searchRepo = new SearchRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("app");
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json([
            'data' => $this->searchRepo->store($request->all())
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history()
    {
        return response()->json([
            'data' => $this->searchRepo->history()
        ], 200);
    }
}
