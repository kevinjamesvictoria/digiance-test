import Search from './vue/pages/Search';
import History from './vue/pages/History';

export const routes = [
    {
        name: 'search',
        path: '/',
        component: Search
    },
    {
        name: 'search_history',
        path: '/history',
        component: History
    },
];