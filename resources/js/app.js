require('./bootstrap');


import Vue from 'vue/dist/vue';

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueRouter from 'vue-router';
import App from './vue/app'
import { routes } from './routes';
// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);
window.Vue = Vue;
Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: routes
});
const app = new Vue({
   el: '#app',
   router: router,
   render: h => h(App)
});
