<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', [MainController::class, 'index']);
Route::prefix('/search')->group(function (){
    Route::post('/store', [MainController::class, 'store']);
    Route::get('/history', [MainController::class, 'history']);
});
